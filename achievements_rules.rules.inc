<?php

/**
 * @file
 */

/**
 * Implements hook_rules_action_info().
 *
 * Declares any meta-data about actions for Rules in a big, associative, nested
 * array. See also hook_rules_action_info in the rules.api.php file, and the
 * online documentation at http://drupal.org/node/878928.
 */
function achievements_rules_rules_action_info() {
  $actions = array(
    'achievements_rules_unlock' => array(
      'label' => t('Achievements: Unlock '),
      'group' => t('Achievements'),
      'parameter' => array(
        'achievement' => array(
          'type' => 'text',
          'label' => t('Achievement to unlock'),
        ),
      ),
    ),
    'achievements_rules_lock' => array(
      'label' => t('Achievements: Unlock remove '),
      'group' => t('Achievements'),
      'parameter' => array(
        'achievement' => array(
          'type' => 'text',
          'label' => t('Achievement unlock to remove'),
        ),
      ),
    ),

    // Example code: grant a hard wired achievement,
    // named scientist and superbrain
    // uncomment the following Lines to enable
    /*
    'achievements_rules_grantscientist' => array(
      'label' => t('Achievements: Grant scientist'),
      'group' => t('Achievements'),
    ),
    'achievements_rules_grantsuperbrain' => array(
      'label' => t('Achievements: Grant superbrain'),
      'group' => t('Achievements'),
    ),
    */
  );

  return $actions;
}


/**
 * The action functions for the above
 */
function achievements_rules_unlock($achievement) {
  global $user;
  $achievement=check_plain($achievement);   // reduce risks
  if ( strlen($achievement)>0 ) {
    watchdog('achievements_rules', 'action: unlock achievement=' . $achievement);
    achievements_unlocked($achievement, $user->uid);
  }
  else {
    watchdog('achievements_rules', 'action: ignore empty achievement');
  }
}

function achievements_rules_lock($achievement) {
  global $user;
  $achievement=check_plain($achievement);   // reduce risks
  if ( strlen($achievement)>0 ) {
    watchdog('achievements_rules', 'action: take away unlock achievement=' . $achievement);
    achievements_locked($achievement, $user->uid);
  }
  else {
    watchdog('achievements_rules', 'action: ignore empty achievement');
  }
}


function achievements_rules_grantscientist() {
  global $user;
  achievements_unlocked('scientist', $user->uid);
}
function achievements_rules_grantsuperbrain() {
  global $user;
  achievements_unlocked('superbrain', $user->uid);
}

